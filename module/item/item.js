/**
 * Extend the basic Item with some very simple modifications.
 * @extends {Item}
 */
export class LibreteItem extends Item {
  /**
   * Augment the basic Item data model with additional dynamic data.
   */
  prepareData() {
    super.prepareData();

    // Get the Item's data
    const itemData = this.data;
    const actorData = this.actor ? this.actor.data : {};
    const data = itemData.data;
  }

  jetDeDes (mise,nom){
    // Basic template rendering data
    const item = this.data;
    const actorData = this.actor ? this.actor.data.data : {};
    const itemData = item.data;

    if (mise > actorData.bile.value) {
      mise = actorData.bile.value ;
    }
    let roll = new Roll('2d6+'+mise, actorData);
    let label =  this.actor.data.name +' mise '+ mise + ' bile noire pour '+nom ;
    roll.roll().toMessage({
      speaker: ChatMessage.getSpeaker({ actor: this.actor }),
      flavor: label
    });

    let resJet = roll.results[0] + Number(mise) ;
    
    if ( resJet > 7 ) {
       let b = actorData.bile.value - Number(mise) ;
       this.actor.update({'data.bile.value' : b} );
    }
  }

  /**
   * Handle clickable rolls.
   * @param {Event} event   The originating click event
   * @private
   */
  async roll() {
    
    // Basic template rendering data
    const token = this.actor.token;
    const item = this.data;
    const actorData = this.actor ? this.actor.data.data : {};
    const itemData = item.data;

    // Vérifier que c'est une manoeuvre qui est lancée
    if(!(item.type === "enfantillage")) return ui.notifications.warn("Seul en enfantillage peut être utilisé");
    let mise = 2 ;

    // Demande dans une fenêtre la mise souhaitée
    let d = new Dialog({
      title: "Mise de bile noire " + item.name,
      content: "<p>Pour cette enfantillage ("+item.name+"), vous devez choisir une mise de bile :</p>",
      buttons: {
       one: {
        icon: '<i class="far fa-circle"></i>',
        label: "Aucune bile",
        callback: () =>this.jetDeDes(0, item.name)
       },
       two: {
        icon: '<i class="fas fa-circle"></i>',
        label: "Une bile",
        callback: () => this.jetDeDes(1, item.name)
       },
       tree: {
        icon: '<i class="fas fa-circle"></i><i class="fas fa-circle"></i>',
        label: "Deux biles",
        callback: () => this.jetDeDes(2, item.name)
       },
       four: {
        icon: '<i class="fas fa-circle"></i><i class="fas fa-circle"></i><i class="fas fa-circle"></i>',
        label: "Trois biles",
        callback: () => this.jetDeDes(3, item.name)
       }
      },
      default: "one",
     });
     d.render(true);
    // 
    
    

    //-------
    }
    
  
}
