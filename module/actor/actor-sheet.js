/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class LibreteActorSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["librete", "sheet", "actor"],
      width: 600,
      height: 750,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "manoeuvre" }]
    });
  }

  /** @override */
  get template() {
    const path = "systems/librete/templates/actor";
    // Return a single sheet for all item types.
    // return `${path}/item-sheet.html`;

    // Alternatively, you could use the following return statement to do a
    // unique item sheet by type, like `weapon-sheet.html`.
    return `${path}/actor-${this.actor.data.type}-sheet.html`;
  }
  

  /* -------------------------------------------- */

  /** @override */
  getData() {
    const data = super.getData();

    data.dtypes = ["String", "Number", "Boolean"];
    //for (let attr of Object.values(data.data.attributes)) {
    //  attr.isCheckbox = attr.dtype === "Boolean";
    //}

    // Prepare items.
    if (this.actor.data.type == 'enfant') {
      this._prepareCharacterItems(data);
    }
    return data;
  }

  /**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
  _prepareCharacterItems(sheetData) {
    const actorData = sheetData.actor;

    // Initialize containers.
    const manoeuvre = [] ;
    const objet = [];
    

    // Iterate through items, allocating to containers
    // let totalWeight = 0;
    for (let i of sheetData.items) {
      let item = i.data;
      i.img = i.img || DEFAULT_TOKEN;
      // Append to manoeuvre.
      if (i.type === 'enfantillage') {
        manoeuvre.push(i);
      }
      // Append to objet.
      if (i.type === 'objet') {
        objet.push(i);
      }
    }

    // Assign and return
    actorData.objet = objet;
    actorData.manoeuvre = manoeuvre;
  }

  /* -------------------------------------------- */

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Gain de bile
    html.find('.gain-bile').click(this._gainBile.bind(this));

    //Mise de bile
    html.find('.jetDe').click(this._miseBile.bind(this));

    // Add Inventory Item
    html.find('.item-create').click(this._onItemCreate.bind(this));

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getOwnedItem(li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.deleteOwnedItem(li.data("itemId"));
      li.slideUp(200, () => this.render(false));
    });

    // Drag events for macros.
    if (this.actor.owner) {
      let handler = ev => this._onDragStart(ev);
      html.find('li.item').each((i, li) => {
        if (li.classList.contains("inventory-header")) return;
        li.setAttribute("draggable", true);
        li.addEventListener("dragstart", handler, false);
      });
    }
  }

  /**
   * Handle creating a new Owned Item for the actor using initial data defined in the HTML dataset
   * @param {Event} event   The originating click event
   * @private
   */
  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    // Get the type of item to create.
    const type = header.dataset.type;
    // Grab any data associated with this control.
    const data = duplicate(header.dataset);
    // Initialize a default name.
    const name = `New ${type.capitalize()}`;
    // Prepare the item object.
    const itemData = {
      name: name,
      type: type,
      data: data
    };
    // Remove the type from the dataset since it's in the itemData.type prop.
    delete itemData.data["type"];

    // Finally, create the item!
    return this.actor.createOwnedItem(itemData);
  }

  
  /**
   * Gaine de bile
   * @param {Event} event
   * @private
   */

  _gainBile(event){
/**
   * Fonction pour prendre de la bile 
   * 
   * L'idée est de mettre trois boutons (1 à 3) qui appelle la même fonction mais avec un paramètre de gain de bile différent
   */
    event.preventDefault();
    const element = event.currentTarget ;
    const dataset = element.dataset ;

    //Mise à jour de la valeur de la bile dans l'Acteur
    let b ;
    b = this.actor.data.data.bile.value + Number(dataset.gain) ;
    this.actor.update({'data.bile.value' : b} );

    ChatMessage.create(
      {content: this.actor.name +" gagne "+dataset.gain+" bile noire",
       speaker:ChatMessage.getSpeaker({actor: this.actor}),
       type:CHAT_MESSAGE_TYPES.EMOTE,
    },
    {chatBubble: true }
    );
  }

  /**
   * Mise de bile
   * @param {'Event} event
   * @private
   */
  _miseBile(event){
    /**
       * Fonction pour prendre de la bile 
       * 
       * L'idée est de mettre trois boutons (1 à 3) qui appelle la même fonction mais avec un paramètre de gain de bile différent
       */
        event.preventDefault();
        const element = event.currentTarget ;
        const dataset = element.dataset ;
        const donnee = this.actor.data.data ;    

        //Vérifier que la mise de bile est possible en fonction de la valeur de la mise (au moins au maximum de ce qui est possible)
        let mise = dataset.mise ;
        if (mise > donnee.bile.value) {
          mise = donnee.bile.value ;
        }

        //Obtenir le nom de la manoeuvre : le "item-id" du dataset permet d'avoir toute les info nécessaires
        let id = $(element).parents(".item").data("item-id") ;
        const item = this.actor.getOwnedItem(id);

        // Préparation du message

        let message = this.actor.name + " mise "+ mise + " bile noire pour " + item.name ;

        //mise à jour de la bile
        

        const jet  = new Roll( "2d6 + "+mise, this.actor.data.data);
        jet.roll() ;
        
        // Si le jet est réussi, il y a perte de bile
        let resJet = jet.results[0] + Number(mise) ;
        
        if ( resJet > 7 ) {
          let b = donnee.bile.value - Number(mise) ;
          this.actor.update({'data.bile.value' : b} );
        }

        jet.toMessage({
          speaker: ChatMessage.getSpeaker({ actor: this.actor }),
          flavor: message
        }) ;
       
  }
}
